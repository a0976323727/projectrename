package Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

public class ProjectRenameUtil_v2 {

	// RunMode
	public static String RunMode = "";
	// 設定檔Properties
	public static Properties UtilConfig = new Properties();
	// 要轉換系統的舊路徑
	public static String oldPath = "";
	// 要放到的新路徑
	public static String newPath = "";
	// 要合併舊系統的路徑
	public static String oldSysPath = "";
	// 舊資料夾名稱
	public static String oldFloderName = "";
	// 新資料夾名稱
	public static String newFloderName = "";
	// 舊系統資料夾名稱
	public static String oldSystemFloderName = "";
	// 要讀取的專案名稱
	public static String readProjectName = "";
	// 要變成的專案名稱
	public static String newProjectName = "";
	// 舊系統的專案名稱
	public static String oldSystemProjectName = "";
	// 新專案的api路徑
	public static String newProjectApiPath = "";
	// 舊系統的api路徑
	public static String oldSystemProjectApiPath = "";
	// 新專案的Batch路徑
	public static String newProjectBatchPath = "";
	// 舊系統的Batch路徑
	public static String oldSystemProjectBatchPath = "";
	// 新專案的Core路徑
	public static String newProjectCorePath = "";
	// 舊系統的Core路徑
	public static String oldSystemProjectCorePath = "";
	// 新專案的Ws路徑
	public static String newProjectWsPath = "";
	// 舊系統的Ws路徑
	public static String oldSystemProjectWsPath = "";
	// 新專案的web路徑
	public static String newProjectWebPath = "";
	// 舊系統的web路徑
	public static String oldSystemProjectWebPath = "";
	// 新專案的Webapp路徑
	public static String newProjectWebappPath = "";
	// 舊系統的WebContent路徑
	public static String oldSystemProjectWebContentPath = "";

	// 舊系統專案來源是否為maven
	public static boolean isMavenPath = true;
	// 新系統路徑List
	public static List<String> newProjectPaths = new ArrayList<String>();
	// 舊系統路徑List
	public static List<String> oldSystemProjectPaths = new ArrayList<String>();

	// 轉換要讀取的檔案類型
	public static String AcceptFileType[] = { ".xml", ".txt", ".java", ".rar", ".zip", ".properties", ".prefs",
			".component", ".classpath", ".project", ".jsdtscope", ".container", ".name", ".mf", ".jsp", ".js", ".png",
			".jpg", ".sql", ".css", ".cab", ".jrxml", ".jasper", ".gitignore", ".md" };
	// 合併舊系統要讀取的檔案類型
	public static String mergeAcceptFileType[] = { ".xml", ".java", ".jsp", ".jrxml", ".jasper", ".cnf", ".html" };

	// 檔案暫存List<File>
	public static List<File> allFiles = new ArrayList<File>();
	// 檔案內容暫存變數
	public static String fileContentTmp = "";
	//
	public static String mergeNewFilePath = "";

	/**
	 * 執行方法 run
	 * 
	 * @param mode 執行模式: convert:轉換系統 merge:合併舊系統
	 */
	public static void run(String mode) {
		RunMode = mode;
		System.out.println("【RUN MODE】-----" + RunMode + "-----【RUN MODE】");
		// 讀取設定檔
		UtilConfig = loadProperties(UtilConfig, "UtilConfig");
		initPaths();
		try {
			if ("convert".equals(mode)) {
				System.out.println("**************************Start convertProject**************************");
				convertProject(oldPath);
				System.out.println("***************************End convertProject***************************");
			} else {
				System.out.println("**********************Start mergeOldAndNewProject***********************");
				initMergePaths();
				for (int i = 0; i < oldSystemProjectPaths.size(); i++) {
					mergeOldAndNewProject(oldSystemProjectPaths.get(i));
				}
				System.out.println("**********************End mergeOldAndNewProject***********************");
			}
			System.out.println("Success!!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("【ERROR】" + e.getMessage());
		}
	}

	/**
	 * 合併舊系統到轉好的新系統 mergeOldAndNewProject
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void mergeOldAndNewProject(String path) throws Exception {

		File[] oldSys_ScanFiles = new File(path).listFiles();
		try {
			for (File f : oldSys_ScanFiles) {
				if (f.isDirectory()) {
					mergeFileAndFloderRename(f, oldSystemProjectName, newProjectName, "Floder", oldSystemFloderName,
							newFloderName);
					mergeOldAndNewProject(f.getPath());
				}
				if (f.isFile()) {
					allFiles.add(f);
				}
			}
			// End for loop
			// do trans file content
			if (allFiles.size() > 0) {
				fileContextTrans(allFiles, mergeAcceptFileType, oldSystemProjectName, newProjectName,
						oldSystemFloderName, newFloderName);
				allFiles = new ArrayList<File>();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"allFile\" Err : " + e.getMessage());
		}
	}

	/**
	 * 讀取設定檔
	 * 
	 * @param properties
	 * @param propertiesFileName 設定檔 檔案名稱
	 * @return
	 */
	public static Properties loadProperties(Properties properties, String propertiesFileName) {

		String configFilePath = "src/configure/" + propertiesFileName + ".properties";
		try {
			properties.load(new FileInputStream(new File(configFilePath)));
		} catch (FileNotFoundException e) {
			System.out.println("【ERROR】" + configFilePath + ",  " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("【ERROR】" + configFilePath + ",  " + e.getMessage());
			e.printStackTrace();
		}
		return properties;
	}

	/**
	 * 讀取設定檔路徑
	 */
	public static void initPaths() {
		System.out.println("--------------------Init Paths--------------------");
		/**
		 * 印出設定檔資訊
		 */
		// 要轉換系統的舊路徑
		oldPath = UtilConfig.getProperty("oldPath");
		System.out.println("-----oldPath: " + oldPath);
		// 要放到的新路徑
		newPath = UtilConfig.getProperty("newPath");
		System.out.println("-----newPath: " + newPath);
		// 要合併舊系統的路徑
		oldSysPath = UtilConfig.getProperty("oldSysPath");
		System.out.println("-----oldSysPath: " + oldSysPath);
		// 要轉換系統的資料夾名稱
		oldFloderName = UtilConfig.getProperty("oldFloderName");
		System.out.println("-----oldFloderName: " + oldFloderName);
		// 要放到新路徑的資料夾名稱
		newFloderName = UtilConfig.getProperty("newFloderName");
		System.out.println("-----newFloderName: " + newFloderName);
		// 要合併舊系統的資料夾名稱
		oldSystemFloderName = UtilConfig.getProperty("oldSystemFloderName");
		System.out.println("-----oldSystemFloderName: " + oldSystemFloderName);
		// 要讀取的專案名稱
		readProjectName = UtilConfig.getProperty("readProjectName");
		System.out.println("-----readProjectName: " + readProjectName);
		// 要變成的專案名稱
		newProjectName = UtilConfig.getProperty("newProjectName");
		System.out.println("-----newProjectName: " + newProjectName);
		// 舊系統的專案名稱
		oldSystemProjectName = UtilConfig.getProperty("oldSystemProjectName");
		System.out.println("-----oldSystemProjectName: " + oldSystemProjectName);
		System.out.println("--------------------Init Paths--------------------");
	}

	/**
	 * 轉出系統與合併舊系統path初始化設定 initMergePaths
	 */
	public static void initMergePaths() {

		/**
		 * 轉出系統與合併舊系統path設定
		 */
		// 轉出系統的Api 路徑
		newProjectApiPath = UtilConfig.getProperty("newProjectApiPath");
		newProjectPaths.add(newProjectApiPath);
		// 合併舊系統的Api 路徑
		oldSystemProjectApiPath = UtilConfig.getProperty("oldSystemProjectApiSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectApiPath);

		// 轉出系統的Batch 路徑
		newProjectBatchPath = UtilConfig.getProperty("newProjectBatchPath");
		newProjectPaths.add(newProjectBatchPath);
		// 合併舊系統的Batch 路徑
		oldSystemProjectBatchPath = UtilConfig.getProperty("oldSystemProjectBatchSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectBatchPath);

		// 轉出系統的Core 路徑
		newProjectCorePath = UtilConfig.getProperty("newProjectCorePath");
		newProjectPaths.add(newProjectCorePath);
		// 合併舊系統的Core 路徑
		oldSystemProjectCorePath = UtilConfig.getProperty("oldSystemProjectCoreSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectCorePath);

		// 轉出系統的Ws 路徑
		newProjectWsPath = UtilConfig.getProperty("newProjectWsPath");
		newProjectPaths.add(newProjectWsPath);
		// 合併舊系統的Ws 路徑
		oldSystemProjectWsPath = UtilConfig.getProperty("oldSystemProjectWsSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWsPath);

		// 轉出系統的Web 路徑
		newProjectWebPath = UtilConfig.getProperty("newProjectWebPath");
		newProjectPaths.add(newProjectWebPath);
		// 合併舊系統的Web 路徑
		oldSystemProjectWebPath = UtilConfig.getProperty("oldSystemProjectWebSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWebPath);

		// 轉出系統的Webapp 路徑
		newProjectWebappPath = UtilConfig.getProperty("newProjectWebappPath");
		newProjectPaths.add(newProjectWebappPath);
		// 合併舊系統的Webapp 路徑
		oldSystemProjectWebContentPath = UtilConfig.getProperty("oldSystemProjectWebContentSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWebContentPath);

		try {
			checkPaths(newProjectPaths, oldSystemProjectPaths);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 置換路徑String
	 * 
	 * @param Paths
	 * @return
	 */
	public static List<String> replacePaths(List<String> Paths) {
		String thisPath = "";
		for (int i = 0; i < Paths.size(); i++) {
			thisPath = Paths.get(i);
			if (thisPath.contains("#upperCaseNEWSYSID#")) {
				thisPath = thisPath.replace("#upperCaseNEWSYSID#", newProjectName);
			}
			if (thisPath.contains("#lowerCaseNEWSYSID#")) {
				thisPath = thisPath.replace("#lowerCaseNEWSYSID#", newProjectName.toLowerCase());
			}
			if (thisPath.contains("#upperCaseOLDSYSID#")) {
				thisPath = thisPath.replace("#upperCaseOLDSYSID#", oldSystemProjectName);
			}
			if (thisPath.contains("#lowerCaseOLDSYSID#")) {
				thisPath = thisPath.replace("#lowerCaseOLDSYSID#", oldSystemProjectName.toLowerCase());
			}
			Paths.set(i, thisPath);
		}
		return Paths;
	}

	/**
	 * 檢查轉出系統與合併舊系統的路徑
	 * 
	 * @param newProjectPaths
	 * @param oldSystemProjectPaths
	 * @throws Exception
	 */
	public static void checkPaths(List<String> newProjectPaths, List<String> oldSystemProjectPaths) throws Exception {
		try {
			// 目錄文字置換
			newProjectPaths = replacePaths(newProjectPaths);
			oldSystemProjectPaths = replacePaths(oldSystemProjectPaths);
			if (newProjectPaths.size() == 0)
				throw new Exception("【ERROR】newProjectPaths List is empty !!!!");
			if (oldSystemProjectPaths.size() == 0)
				throw new Exception("【ERROR】oldSystemProjectPaths List is empty !!!!");

			isMavenPath = false;
			for (int i = 0; i < 6; i++) {
				String New_p = newProjectPaths.get(i);
				if (!(new File(New_p).exists())) {
					throw new Exception("【ERROR】" + New_p + ":  Not found!!!");
				}
				String Old_p = oldSystemProjectPaths.get(i);
				if (!(new File(Old_p).exists())) {
					isMavenPath = true;
				}
			}
			if (isMavenPath == true) {
				oldSystemProjectPaths.set(0, UtilConfig.getProperty("oldSystemProjectApiMavenPath"));
				oldSystemProjectPaths.set(1, UtilConfig.getProperty("oldSystemProjectBatchMavenPath"));
				oldSystemProjectPaths.set(2, UtilConfig.getProperty("oldSystemProjectCoreMavenPath"));
				oldSystemProjectPaths.set(3, UtilConfig.getProperty("oldSystemProjectWsMavenPath"));
				oldSystemProjectPaths.set(4, UtilConfig.getProperty("oldSystemProjectWebMavenPath"));
				oldSystemProjectPaths.set(5, UtilConfig.getProperty("oldSystemProjectWebContentMavenPath"));
				oldSystemProjectPaths = replacePaths(oldSystemProjectPaths);
			}
			newProjectApiPath = newProjectPaths.get(0);
			System.out.println("-----newProjectApiPath: " + newProjectApiPath);

			oldSystemProjectApiPath = oldSystemProjectPaths.get(0);
			System.out.println("-----oldSystemProjectApiPath: " + oldSystemProjectApiPath);

			newProjectBatchPath = newProjectPaths.get(1);
			System.out.println("-----newProjectBatchPath: " + newProjectBatchPath);

			oldSystemProjectBatchPath = oldSystemProjectPaths.get(1);
			System.out.println("-----oldSystemProjectBatchPath: " + oldSystemProjectBatchPath);

			newProjectCorePath = newProjectPaths.get(2);
			System.out.println("-----newProjectCorePath: " + newProjectCorePath);

			oldSystemProjectCorePath = oldSystemProjectPaths.get(2);
			System.out.println("-----oldSystemProjectCorePath: " + oldSystemProjectCorePath);

			newProjectWsPath = newProjectPaths.get(3);
			System.out.println("-----newProjectWsPath: " + newProjectWsPath);

			oldSystemProjectWsPath = oldSystemProjectPaths.get(3);
			System.out.println("-----oldSystemProjectWsPath: " + oldSystemProjectWsPath);

			newProjectWebPath = newProjectPaths.get(4);
			System.out.println("-----newProjectWebPath: " + newProjectWebPath);

			oldSystemProjectWebPath = oldSystemProjectPaths.get(4);
			System.out.println("-----oldSystemProjectWebPath: " + oldSystemProjectWebPath);

			newProjectWebappPath = newProjectPaths.get(5);
			System.out.println("-----newProjectWebappPath: " + newProjectWebappPath);

			oldSystemProjectWebContentPath = oldSystemProjectPaths.get(5);
			System.out.println("-----oldSystemProjectWebContentPath: " + oldSystemProjectWebContentPath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("【ERROR】: " + e.getMessage());
		}
	}

	/**
	 * 轉換系統 convertProject 如 EXT -> VBD
	 * 
	 * @param path 路徑
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void convertProject(String path) throws Exception {
		// 取得該目錄底下所有資料
		File[] Files = new File(path).listFiles();
		try {
			for (File f : Files) {
				// 如果是資料夾
				if (f.isDirectory()) {
					fileAndFloderRename(f, readProjectName, newProjectName, "Floder", oldFloderName, newFloderName);
					convertProject(f.getPath());
				}
				// 如果是檔案
				if (f.isFile()) {
					allFiles.add(f);
				}
			}
			// End for loop
			// do trans file content
			if (allFiles.size() > 0) {
				fileContextTrans(allFiles, AcceptFileType, readProjectName, newProjectName, oldFloderName,
						newFloderName);
				allFiles = new ArrayList<File>();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("【ERROR】Method \"convertProject\" Err : " + e.getMessage());
		}
	}

	/**
	 * 檔案及資料夾重新命名方法 fileAndFloderRename
	 * 
	 * @param file              傳進來的檔案 or 資料夾
	 * @param beforeProjectName 改名前的專案名稱
	 * @param afterProjectName  改名後的專案名稱
	 * @param fileType          檔案類型 ( 檔案 or 資料夾)
	 * @throws Exception
	 */
	public static void fileAndFloderRename(File file, String beforeProjectName, String afterProjectName,
			String fileType, String beforeFloderName, String afterFloderName) throws Exception {
		try {
			String fileName = file.getName();
			String oldFilePath = file.getPath();
			// newPath
			String newFilePath = "";
			newFilePath = oldFilePath.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
					.replace(beforeProjectName, afterProjectName).replace(beforeFloderName, afterFloderName);
			// 因為是以ext轉為其他系統 排除一些檔案名稱英文單字問題
			newFilePath = fileAndFloderNameReplaceCheck(newFilePath, beforeProjectName, afterProjectName);
			File newFile = null;
			File newDirPath = new File(newPath);
			if (!newDirPath.exists()) {
				newDirPath.mkdir();
			}
			fileAndFloderRenameChkFileType(file, fileType, fileName, beforeProjectName, newFile, newFilePath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("【ERROR】Method \"fileRename\" Err : " + e.getMessage());
		}
	}

	/**
	 * 因為是以ext轉為其他系統 排除一些檔案名稱英文單字問題 fileAndFloderNameReplaceCheck
	 * 
	 * @param fileOrFloderName
	 * @return
	 */
	public static String fileAndFloderNameReplaceCheck(String Path, String beforeProjectName, String afterProjectName) {
		String rtnPath = "";
		String fileName = "";
		if (!"".equals(Path)) {
			fileName = Path.substring(Path.lastIndexOf("\\") + 1, Path.length());
			if ("ext".equals(beforeProjectName.toLowerCase())) {
				if (fileName.contains(afterProjectName.toLowerCase() + "end") || fileName.contains(afterProjectName.toLowerCase() + "ern")) {
					fileName = fileName.replace(afterProjectName.toLowerCase() + "end",
							beforeProjectName.toLowerCase() + "end");
					fileName = fileName.replace(afterProjectName.toLowerCase() + "ern",
							beforeProjectName.toLowerCase() + "ern");
				}
				if (fileName.contains("T" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("T" + afterProjectName.toLowerCase(),
							"T" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("t" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("t" + afterProjectName.toLowerCase(),
							"t" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("N" + afterProjectName)) {
					fileName = fileName.replace("N" + afterProjectName.toLowerCase(),
							"N" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("n" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("n" + afterProjectName.toLowerCase(),
							"n" + beforeProjectName.toLowerCase());
				}
				if(fileName.contains("Ext")) {
					fileName = fileName.replace("Ext", afterProjectName.substring(0,1).toUpperCase()+afterProjectName.substring(1,afterProjectName.length()).toLowerCase());
				}
			}
			rtnPath = Path.substring(0, Path.lastIndexOf("\\") + 1) + fileName;
		}

		return rtnPath;
	}

	/**
	 * 檔案及資料夾改名(建立) 方法 fileAndFloderRenameChkFileType
	 * 
	 * @param file              檔案
	 * @param fileType          檔案類型
	 * @param fileName          檔案名稱
	 * @param beforeProjectName 改名前專案名稱
	 * @param newFile           新檔案
	 * @param newFilePath       新檔案路徑
	 */
	public static void fileAndFloderRenameChkFileType(File file, String fileType, String fileName,
			String beforeProjectName, File newFile, String newFilePath) {
		try {
			if ("Floder".equals(fileType)) {
				// ex: EXT => VBD
				if (fileName.contains(beforeProjectName)) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println(
							"Floder Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				}
				// ex: ext => vbd
				else if (fileName.contains(beforeProjectName.toLowerCase())) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println(
							"Floder Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				
				}
				// ex : Ext => Vbd
				else if (fileName.contains(beforeProjectName.substring(0,1).toUpperCase()+beforeProjectName.substring(1,beforeProjectName.length()).toLowerCase())) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println(
							"Floder Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				}
				// Other name
				else {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println("The Floder 【 " + fileName + " 】 no needed rename! 【 " + file.getPath()
							+ " 】 to 【 " + newFile.getPath() + " 】。");
				}
			} else {
				// Type == File
				// ex: EXT => VBD
				if (fileName.contains(beforeProjectName)) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println(
							"File Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				}
				// ex: ext => vbd
				else if (fileName.contains(beforeProjectName.toLowerCase())) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println(
							"File Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				
				
				} 
				// ex: Ext => Vbd
				else if(fileName.contains(beforeProjectName.substring(0,1).toUpperCase()+beforeProjectName.substring(1,beforeProjectName.length()).toLowerCase())) { 
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println(
							"File Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				
				}
				// Other name	
				else {
				
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println("The File 【 " + fileName + " 】 no needed rename! 【 " + file.getPath()
							+ " 】 to 【 " + newFile.getPath() + " 】。");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(
					"【ERROR】【fileAndFloderRenameChkFileTypeErr】:" + e.getMessage() + " : " + newFile.getPath());
		}

	}

	/**
	 * 合併舊系統的檔案及資料夾改名方法 mergeFileAndFloderRename
	 * 
	 * @param file              檔案 or 資料夾
	 * @param beforeProjectName 改名前專案名稱
	 * @param afterProjectName  改名後專案名稱
	 * @param fileType          檔案類型
	 * @param beforeFloderName  改名前資料夾名稱
	 * @param afterFloderName   改名後資料夾名稱
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void mergeFileAndFloderRename(File file, String beforeProjectName, String afterProjectName,
			String fileType, String beforeFloderName, String afterFloderName) throws Exception {
		try {
			String oldfileName = file.getName();
			String oldFilePath = file.getPath().replace("\\", "/");
			String newFilePath = "";
			File newFile = null;
			for (int i = 0; i < oldSystemProjectPaths.size(); i++) {
				if (oldFilePath.contains(oldSystemProjectPaths.get(i))) {
					newFilePath = oldFilePath.replace(oldSystemProjectPaths.get(i), newProjectPaths.get(i));
					newFilePath = newFilePath.replace(beforeProjectName, afterProjectName)
							.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
							.replace("YBD", "VBD").replace("ybd", "vbd").replace("YCM", "VCM").replace("ycm", "vcm")
							.replace("YCT", "VCT").replace("yct", "vct").replace("YDD", "VDD").replace("ydd", "vdd")
							.replace("YIM", "VIM").replace("yim", "vim").replace("YPM", "VPM").replace("ypm", "vpm")
							.replace("YST", "VST").replace("yst", "vst").replace("Svst", "Syst");
					mergeNewFilePath = newFilePath;
					break;
				}
			}
			fileAndFloderRenameChkFileType(file, fileType, oldfileName, beforeProjectName, newFile, newFilePath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"fileRename\" Err : " + e.getMessage());
		}
	}

	/**
	 * 檔案內容轉換
	 * 
	 * @param allFiles          檔案List
	 * @param AcceptFileType    接受的檔案類型
	 * @param beforeProjectName 改名前的專案名稱
	 * @param afterProjectName  改名後的專案名稱
	 * @param beforeFloderName  改名後的資料夾名稱
	 * @param afterFloderName   改名後資料夾名稱
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void fileContextTrans(List<File> allFiles, String AcceptFileType[], String beforeProjectName,
			String afterProjectName, String beforeFloderName, String afterFloderName) throws Exception {
		// 目前接受的檔案類型
		String acpFileType = "";
		// 這個檔案的檔案類型
		String thisFileType = "";

		// 寫檔用
		BufferedReader br = null;
		String line = "";
		FileWriter fw = null;

		try {
			// 如果接受檔案類型陣列為空
			if (AcceptFileType.length == 0) {
				throw new Exception("【ERROR】AcceptFileType's length is empty !!");
			} else {
				for (int i = 0; i < AcceptFileType.length; i++) {
					acpFileType = AcceptFileType[i];
					for (File thisFile : allFiles) {
						// 取出目前檔案的檔案類型
						thisFileType = thisFile.getName().substring(thisFile.getName().lastIndexOf("."),
								thisFile.getName().length());
						thisFileType = thisFileType.toLowerCase();
						// 如果目前檔案的檔案類型 與 這次迴圈的接受檔案類型不符合則continue跳到下一次迴圈
						if (!acpFileType.equals(thisFileType)) {
							continue;
						} else {
							// 如果是以下檔案類型直接複製檔案
							// if file is .rar&zip&png&jpg&css&js&cab type
							if (".rar".equals(thisFileType) || ".zip".equals(thisFileType)
									|| ".png".equals(thisFileType) || ".jpg".equals(thisFileType)
									|| ".css".equals(thisFileType) || ".js".equals(thisFileType)
									|| ".cab".equals(thisFileType) || ".jrxml".equals(thisFileType)
									|| ".jasper".equals(thisFileType)) {
								String thisFilePath = thisFile.getPath();
								String newFilePaths = thisFile.getPath().replace(beforeProjectName, afterProjectName)
										.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
										.replace(beforeFloderName, afterFloderName);
								newFilePaths = fileAndFloderNameReplaceCheck(newFilePaths, beforeProjectName,
										afterProjectName);
								File oldFile = new File(thisFilePath);
								if (!oldFile.exists()) {
									throw new Exception("【ERROR】old file: " + oldFile.getPath() + " is no exists!!");
								}
								File newFile = new File(newFilePaths);
								if (newFile.exists()) {
									newFile.delete();
								}
								try {
									FileUtils.copyFile(oldFile, newFile);
									System.out.println("\"Copy file success!! \" 【 " + oldFile.getPath() + " to 【 "
											+ newFile.getPath() + " 】 。");
									continue;
								} catch (Exception e) {
									e.printStackTrace();
									System.out.println("【ERROR】\"Copy file failed!! \"" + e.getMessage());
								}
							}
							// 檔案名稱改名
							if (thisFile.getName().contains(beforeProjectName)
									|| thisFile.getName().contains(beforeProjectName.toLowerCase())
									|| thisFile != null) {
								// file rename
								if ("merge".equals(RunMode)) {
									mergeFileAndFloderRename(thisFile, beforeProjectName, afterProjectName, "File",
											beforeFloderName, afterFloderName);

								} else {
									fileAndFloderRename(thisFile, beforeProjectName, afterProjectName, "File",
											beforeFloderName, afterFloderName);
								}
							} else {
								throw new Exception("【ERROR】: " + thisFile.getPath() + " 檔案寫檔時，檔案名稱改名錯誤!!");
							}
							File mergeNewFile = new File(mergeNewFilePath);
							if (mergeNewFile.exists() && mergeNewFile.length() > 0) {
								System.out.println("【WARN】【 略過 新系統: " + newProjectName + " 目錄: " + mergeNewFilePath
										+ "已有同名的檔案 !! 】");
								continue;
							}
							br = new BufferedReader(new FileReader(thisFile));
							String lineTmp;
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
							Date d = new Date();
							String toDay = sdf.format(d);
							while ((line = br.readLine()) != null) {
								lineTmp = line;
								// line contain "EXT"
								if (lineTmp.contains(beforeProjectName)) {
									lineTmp = lineTmp.replace(beforeProjectName, afterProjectName);
								}
								// line contain "ext"
								if (lineTmp.contains(beforeProjectName.toLowerCase())) {
									lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
											afterProjectName.toLowerCase());
								}
								// line contain "Ext"
								if (lineTmp.contains(beforeProjectName.substring(0,1).toUpperCase()+beforeProjectName.substring(1,beforeProjectName.length()).toLowerCase())) {
									lineTmp = lineTmp.replace(beforeProjectName.substring(0,1).toUpperCase()+beforeProjectName.substring(1,beforeProjectName.length()).toLowerCase(),
											afterProjectName.substring(0,1).toUpperCase()+afterProjectName.substring(1,afterProjectName.length()).toLowerCase());
								}
								lineTmp = lineTmp.replace("YBD", "VBD").replace("ybd", "vbd").replace("YCM", "VCM").replace("ycm", "vcm")
										.replace("YCT", "VCT").replace("yct", "vct").replace("YDD", "VDD").replace("ydd", "vdd")
										.replace("YIM", "VIM").replace("yim", "vim").replace("YPM", "VPM").replace("ypm", "vpm")
										.replace("YST", "VST").replace("yst", "vst").replace("Svst", "Syst").replace("svst", "syst")
										.replace("SVSTEM", "SYSTEM");
								if (lineTmp.contains("@@程式版本")) {
									lineTmp = "@@程式版本 = V1.000";
								}
								if (lineTmp.contains("@@更新日期")) {
									lineTmp = "@@更新日期 = " + toDay;
								}
								if ("ext".equals(beforeProjectName.toLowerCase())) {
									if (lineTmp.contains(afterProjectName.toLowerCase() + "ends") || lineTmp.contains(afterProjectName.toLowerCase() + "ern")) {
										lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
												afterProjectName.toLowerCase());
										lineTmp = lineTmp.replace(afterProjectName.toLowerCase() + "ends", "extends");
										lineTmp = lineTmp.replace(afterProjectName.toLowerCase() + "ern", "extern");
									} else {
										lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
												afterProjectName.toLowerCase());
									}
									if (lineTmp.contains(afterProjectName.toLowerCase() + "en")) {
										lineTmp = lineTmp.replace(afterProjectName.toLowerCase() + "en",
												beforeProjectName.toLowerCase() + "en");
									}
									if (lineTmp.contains("T" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("T" + afterProjectName.toLowerCase(),
												"T" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("T" + afterProjectName.toUpperCase())) {
										lineTmp = lineTmp.replace("T" + afterProjectName.toUpperCase(),
												"T" + beforeProjectName.toUpperCase());
									}
									if (lineTmp.contains("t" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("t" + afterProjectName.toLowerCase(),
												"t" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("N" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("N" + afterProjectName.toLowerCase(),
												"N" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("N" + afterProjectName.toUpperCase())) {
										lineTmp = lineTmp.replace("N" + afterProjectName.toUpperCase(),
												"N" + beforeProjectName.toUpperCase());
									}
									if (lineTmp.contains("n" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("n" + afterProjectName.toLowerCase(),
												"n" + beforeProjectName.toLowerCase());
									}
								}
								// jsp convert
								if (".jsp".equals(thisFileType) || ".js".equals(thisFileType)) {
									if (lineTmp.contains("lts.global.core.util.LTSManagerUtil")) {
										lineTmp = lineTmp.replace("lts.global.core.util.LTSManagerUtil",
												"mitac.global.core.util.MITACManagerUtil");
									}
									if (lineTmp.contains("lts.global.core.util.LTSButton")) {
										lineTmp = lineTmp.replace(
												"lts.global.core.util.LTSButton",
												"mitac." + afterProjectName.toLowerCase() + ".core.util.LTSButton1");
									}
									if (lineTmp.contains("lts." + afterProjectName.toLowerCase() + ".core.util.LTSButton1")) {
										lineTmp = lineTmp.replace(
												"lts."+ afterProjectName.toLowerCase() +".core.util.LTSButton1",
												"mitac." + afterProjectName.toLowerCase() + ".core.util.LTSButton1");
									}
									if (lineTmp.contains("lts."+ afterProjectName.toLowerCase() +".core.util.LTSPreservationField")) {
										lineTmp = lineTmp.replace(
												"lts."+ afterProjectName.toLowerCase() +".core.util.LTSPreservationField",
												"mitac." + afterProjectName.toLowerCase() + ".core.util.LTSPreservationField");
									}
									if (lineTmp.contains("table-redmond")) {
										lineTmp = lineTmp.replace("table-redmond","innerTable");
										lineTmp = lineTmp.replace("style='width:100%'","");
										lineTmp = lineTmp.replace("style='width: 100%'","");
										lineTmp = lineTmp.replace("width=\"100%\"","");
										lineTmp = lineTmp.replace("width= \"100%\"","");
									}
									if (lineTmp.contains("LTSButton Button = new LTSButton")) {
										lineTmp = lineTmp.replace("LTSButton Button = new LTSButton", "LTSButton1 Button = new LTSButton1");
									}
								}
								if (lineTmp.contains("implements lts.")) {
									lineTmp = lineTmp.replace("implements lts.", "implements mitac.");
								}
								if (lineTmp.contains("package lts.")) {
									lineTmp = lineTmp.replace("package lts.", "package mitac.");
								}
								if (lineTmp.contains("import gov.fdc.")) {
									lineTmp = lineTmp.replace("import gov.fdc.", "import mitac.");
								}
								if (lineTmp.contains("import lts.")) {
									lineTmp = lineTmp.replace("import lts.", "import mitac.");
								}
								if (lineTmp.contains("import com.acer")) {
									lineTmp = lineTmp.replace("import com.acer", "import com.mitac");
								}
								if (lineTmp.contains("com.acer")) {
									lineTmp = lineTmp.replace("com.acer", "com.mitac");
								}
								if (lineTmp.contains("FDCTransactionManager")) {
									lineTmp = lineTmp.replace("FDCTransactionManager", "MITACTransactionManager");
								}
								if (lineTmp.contains("LTSDao")) {
									lineTmp = lineTmp.replace("LTSDao", "MITACDao");
								}
								if (lineTmp.contains("extends " + beforeProjectName + "Controller")) {
									lineTmp = lineTmp.replace("extends " + beforeProjectName + "Controller",
											"extends CommonController");
								}
								if (lineTmp.contains("extends " + beforeProjectName.toLowerCase() + "Controller")) {
									lineTmp = lineTmp.replace(
											"extends " + beforeProjectName.toLowerCase() + "Controller",
											"extends CommonController");
								}
								if (lineTmp.contains("gov.fdc.")) {
									lineTmp = lineTmp.replace("gov.fdc.", "mitac.");
								}
//								if (lineTmp.contains("new HashMap() : queryResult.getFirstByMap())")) {
//									lineTmp = lineTmp.replace("new HashMap() : queryResult.getFirstByMap())", "new HashMap() : (HashMap) queryResult.getFirstByMap())");
//								}
								if (lineTmp.contains("FdcThreadHolder")) {
									lineTmp = lineTmp.replace("FdcThreadHolder", "MitacThreadHolder");
								}
								if (lineTmp.contains("LTSAdapter")) {
									lineTmp = lineTmp.replace("LTSAdapter", "Adapter");
								}
								if (lineTmp.contains("LTSApplicationException")) {
									lineTmp = lineTmp.replace("LTSApplicationException", "MITACApplicationException");
								}
								if (lineTmp.contains("LTSDataAccessException")) {
									lineTmp = lineTmp.replace("LTSDataAccessException", "MITACDataAccessException");
								}
								if (lineTmp.contains("LTSManagerImpl")) {
									lineTmp = lineTmp.replace("LTSManagerImpl", "CommonManagerImpl");
								}
								if (lineTmp.contains("LTSWebManagerImpl")) {
									lineTmp = lineTmp.replace("LTSWebManagerImpl", "MITACWebManagerImpl");
								}
								if (lineTmp.contains("LTSWebManager")) {
									lineTmp = lineTmp.replace("LTSWebManager", "MITACWebManager");
								}
								if (lineTmp.contains("LTSManager")) {
									lineTmp = lineTmp.replace("LTSManager", "CommonManager");
								}
								if (lineTmp.contains("LTSLog")) {
									lineTmp = lineTmp.replace("LTSLog", "MITACLog");
								}
								if (lineTmp.contains("LTSLogConstant")) {
									lineTmp = lineTmp.replace("LTSLogConstant", "MITACLogConstant");
								}
								if (lineTmp.contains("LTSController")) {
									lineTmp = lineTmp.replace("LTSController", "MITACController");
								}
								if (lineTmp.contains("LTSDefineException")) {
									lineTmp = lineTmp.replace("LTSDefineException", "MITACDefineException");
								}
								if (lineTmp.contains("import mitac.global.api.adapter.Adapter")) {
									lineTmp = lineTmp.replace("import mitac.global.api.adapter.Adapter",
											"import mitac." + afterProjectName.toLowerCase() + ".api.adapter.Adapter");
								}
								if (lineTmp.contains("import mitac.global.core.manager.CommonManager")) {
									lineTmp = lineTmp.replace("import mitac.global.core.manager.CommonManager",
											"import mitac." + afterProjectName.toLowerCase() + ".core.manager.CommonManager");
								}
								if (lineTmp.contains("import mitac.global.core.manager.impl.CommonManagerImpl")) {
									lineTmp = lineTmp.replace("import mitac.global.core.manager.impl.CommonManagerImpl",
											"import mitac." + afterProjectName.toLowerCase() + ".core.manager.impl.CommonManagerImpl");
								}
								if (lineTmp.contains("public lts.")) {
									lineTmp = lineTmp.replace("public lts.", "public mitac.");
								}
								if (lineTmp.contains("private lts.")) {
									lineTmp = lineTmp.replace("private lts.", "private mitac.");
								}
								fileContentTmp = fileContentTmp + lineTmp + "\n";
							}
						}
						// end while
						try {
							String oldFilePath = thisFile.getPath();
							String newFilePath = "";
							if ("merge".equals(RunMode)) {
								newFilePath = mergeNewFilePath;
							} else {
								newFilePath = oldFilePath
										.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
										.replace(beforeProjectName, afterProjectName)
										.replace(beforeFloderName, afterFloderName);
								newFilePath = fileAndFloderNameReplaceCheck(newFilePath, beforeProjectName,
										afterProjectName);
							}
							File newFile = new File(newFilePath);
							if (!newFile.exists()) {
								newFile.createNewFile();
							}
							fw = new FileWriter(newFile);
							fw.write("");
							fw.flush();
							fw.write(fileContentTmp);
							fw.flush();
							fw.close();
							fileContentTmp = "";
							System.out.println("Write File Success!!! " + newFile.getName());
						} catch (Exception e) {
							e.printStackTrace();
							System.out.println("\"FileWriter\" Err : " + e.getMessage());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"fileContextTrans\" Err : " + e.getMessage());
		}
	}
}
