package Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

public class ProjectRenameUtil {
	
	// RunMode
	public static String RunMode = "";
	// 讀取設定檔
	public static Properties UtilConfig = new Properties();
	// 要轉換系統的舊路徑
	public static String oldPath = "";
	// 要放到的新路徑
	public static String newPath = "";
	// 要合併舊系統的路徑
	public static String oldSysPath = "";
	// 舊資料夾名稱
	public static String oldFloderName = "";
	// 新資料夾名稱
	public static String newFloderName = "";
	// 舊系統資料夾名稱
	public static String oldSystemFloderName = "";
	// 要讀取的專案名稱
	public static String readProjectName = "";
	// 要變成的專案名稱
	public static String newProjectName = "";
	// 舊系統的專案名稱
	public static String oldSystemProjectName = "";
	// 新專案的api路徑
	public static String newProjectApiPath = "";
	// 舊系統的api路徑
	public static String oldSystemProjectApiPath = "";
	// 新專案的Batch路徑
	public static String newProjectBatchPath = "";
	// 舊系統的Batch路徑
	public static String oldSystemProjectBatchPath = "";
	// 新專案的Core路徑
	public static String newProjectCorePath = "";
	// 舊系統的Core路徑
	public static String oldSystemProjectCorePath = "";
	// 新專案的Ws路徑
	public static String newProjectWsPath = "";
	// 舊系統的Ws路徑
	public static String oldSystemProjectWsPath = "";
	// 新專案的web路徑
	public static String newProjectWebPath = "";
	// 舊系統的web路徑
	public static String oldSystemProjectWebPath = "";
	// 新專案的Webapp路徑
	public static String newProjectWebappPath = "";
	// 舊系統的WebContent路徑
	public static String oldSystemProjectWebContentPath = "";

	// 舊系統專案來源是否為maven
	public static boolean isMavenPath = true;
	// 新系統路徑List
	public static List<String> newProjectPaths = new ArrayList<String>();
	// 舊系統路徑List
	public static List<String> oldSystemProjectPaths = new ArrayList<String>();

	public static String mergeNewFilePath = "";

	// init param
	public static void run(String mode) {
		RunMode = mode;

		UtilConfig = loadProperties(UtilConfig, "UtilConfig");
		System.out.println("--------------------Init_Properties--------------------");
		System.out.println("【RUN MODE】-----" + mode + "-----【RUN MODE】");
		oldPath = getOldPath();
		System.out.println("-----oldPath: " + oldPath);
		newPath = getNewPath();
		System.out.println("-----newPath: " + newPath);
		oldSysPath = getNewPath();
		System.out.println("-----oldSysPath: " + oldSysPath);
		oldFloderName = getOldFloderName();
		System.out.println("-----oldFloderName: " + oldFloderName);
		newFloderName = getNewFloderName();
		System.out.println("-----newFloderName: " + newFloderName);
		oldSystemFloderName = getOldSystemFloderName();
		System.out.println("-----oldSystemFloderName: " + oldSystemFloderName);
		readProjectName = getReadProjectName();
		System.out.println("-----readProjectName: " + readProjectName);
		newProjectName = getNewProjectName();
		System.out.println("-----newProjectName: " + newProjectName);
		oldSystemProjectName = getOldSystemProjectName();
		System.out.println("-----oldSystemProjectName: " + oldSystemProjectName);
		System.out.println("--------------------Init_Properties--------------------");
		initPaths();
		try {
			if ("convert".equals(mode)) {
				System.out.println("**************************Start convertProject**************************");
				convertProject(getOldPath());
				System.out.println("***************************End convertProject***************************");
			} else {
				System.out.println("**********************Start mergeOldAndNewProject***********************");
				for (int i = 0; i < oldSystemProjectPaths.size(); i++) {
					mergeOldAndNewProject(oldSystemProjectPaths.get(i));
				}
				System.out.println("**********************End mergeOldAndNewProject***********************");
			}
			System.out.println("Success!!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error!!" + e.getMessage());
		}
	}

	// 要讀取的檔案類型
	public static String AcceptFileType[] = { ".xml", ".txt", ".java", ".rar", ".zip", ".properties", ".prefs",
			".component", ".classpath", ".project", ".jsdtscope", ".container", ".name", ".mf", ".jsp", ".js", ".png",
			".jpg", ".sql", ".css", ".cab", ".jrxml", ".jasper" };
	// 合併舊系統要讀取的檔案類型
	public static String mergeAcceptFileType[] = { ".xml", ".java", ".jsp" };
	// 檔案暫存List<File>
	public static List<File> allFiles = new ArrayList<File>();
	// 檔案內容暫存變數
	public static String fileContentTmp = "";

	/**
	 * initPaths()
	 */
	public static void initPaths() {
		boolean chk = true;
		System.out.println("--------------------Init_Paths--------------------");
		newProjectApiPath = UtilConfig.getProperty("newProjectApiPath");
		newProjectPaths.add(newProjectApiPath);
		oldSystemProjectApiPath = UtilConfig.getProperty("oldSystemProjectApiSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectApiPath);

		newProjectBatchPath = UtilConfig.getProperty("newProjectBatchPath");
		newProjectPaths.add(newProjectBatchPath);
		oldSystemProjectBatchPath = UtilConfig.getProperty("oldSystemProjectBatchSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectBatchPath);

		newProjectCorePath = UtilConfig.getProperty("newProjectCorePath");
		newProjectPaths.add(newProjectCorePath);
		oldSystemProjectCorePath = UtilConfig.getProperty("oldSystemProjectCoreSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectCorePath);

		newProjectWsPath = UtilConfig.getProperty("newProjectWsPath");
		newProjectPaths.add(newProjectWsPath);
		oldSystemProjectWsPath = UtilConfig.getProperty("oldSystemProjectWsSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWsPath);

		newProjectWebPath = UtilConfig.getProperty("newProjectWebPath");
		newProjectPaths.add(newProjectWebPath);
		oldSystemProjectWebPath = UtilConfig.getProperty("oldSystemProjectWebSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWebPath);

		newProjectWebappPath = UtilConfig.getProperty("newProjectWebappPath");
		newProjectPaths.add(newProjectWebappPath);
		oldSystemProjectWebContentPath = UtilConfig.getProperty("oldSystemProjectWebContentSvnPath");
		oldSystemProjectPaths.add(oldSystemProjectWebContentPath);
		try {
			checkPaths(newProjectPaths, oldSystemProjectPaths);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("--------------------Init_Paths--------------------");
	}

	/**
	 * 置換路徑String
	 * 
	 * @param Paths
	 * @return
	 */
	public static List<String> replacePaths(List<String> Paths) {
		String thisPath = "";
		for (int i = 0; i < Paths.size(); i++) {
			thisPath = Paths.get(i);
			if (thisPath.contains("#upperCaseNEWSYSID#")) {
				thisPath = thisPath.replace("#upperCaseNEWSYSID#", newProjectName);
			}
			if (thisPath.contains("#lowerCaseNEWSYSID#")) {
				thisPath = thisPath.replace("#lowerCaseNEWSYSID#", newProjectName.toLowerCase());
			}
			if (thisPath.contains("#upperCaseOLDSYSID#")) {
				thisPath = thisPath.replace("#upperCaseOLDSYSID#", oldSystemProjectName);
			}
			if (thisPath.contains("#lowerCaseOLDSYSID#")) {
				thisPath = thisPath.replace("#lowerCaseOLDSYSID#", oldSystemProjectName.toLowerCase());
			}
			Paths.set(i, thisPath);
		}
		return Paths;
	}

	/**
	 * checkPaths
	 * 
	 * @param path
	 * @throws Exception
	 */
	public static void checkPaths(List<String> newProjectPaths, List<String> oldSystemProjectPaths) throws Exception {
		try {
			// 目錄文字置換
			newProjectPaths = replacePaths(newProjectPaths);
			oldSystemProjectPaths = replacePaths(oldSystemProjectPaths);
			if (newProjectPaths.size() == 0)
				throw new Exception("newProjectPaths List is empty !!!!");
			if (oldSystemProjectPaths.size() == 0)
				throw new Exception("oldSystemProjectPaths List is empty !!!!");

			List<String> mavenPaths = new ArrayList<String>();
			isMavenPath = false;
			for (int i = 0; i < 6; i++) {
				String New_p = newProjectPaths.get(i);
				if (!(new File(New_p).exists())) {
					if (New_p.contains(newProjectName.toLowerCase() + "-batch")) {
						if (!new File(New_p).exists()) {
							new File(New_p).mkdirs();
						}
					} else {
						System.out.println("-----" + New_p + ":  Not found!!!");
					}
				}
				String Old_p = oldSystemProjectPaths.get(i);
				if (!(new File(Old_p).exists())) {
					isMavenPath = true;
				}
			}
			if (isMavenPath == true) {
				oldSystemProjectPaths.set(0, UtilConfig.getProperty("oldSystemProjectApiMavenPath"));
				oldSystemProjectPaths.set(1, UtilConfig.getProperty("oldSystemProjectBatchMavenPath"));
				oldSystemProjectPaths.set(2, UtilConfig.getProperty("oldSystemProjectCoreMavenPath"));
				oldSystemProjectPaths.set(3, UtilConfig.getProperty("oldSystemProjectWsMavenPath"));
				oldSystemProjectPaths.set(4, UtilConfig.getProperty("oldSystemProjectWebMavenPath"));
				oldSystemProjectPaths.set(5, UtilConfig.getProperty("oldSystemProjectWebContentMavenPath"));
				oldSystemProjectPaths = replacePaths(oldSystemProjectPaths);
			}
			newProjectApiPath = newProjectPaths.get(0);
			System.out.println("-----newProjectApiPath: " + newProjectApiPath);

			oldSystemProjectApiPath = oldSystemProjectPaths.get(0);
			System.out.println("-----oldSystemProjectApiPath: " + oldSystemProjectApiPath);

			newProjectBatchPath = newProjectPaths.get(1);
			System.out.println("-----newProjectBatchPath: " + newProjectBatchPath);

			oldSystemProjectBatchPath = oldSystemProjectPaths.get(1);
			System.out.println("-----oldSystemProjectBatchPath: " + oldSystemProjectBatchPath);

			newProjectCorePath = newProjectPaths.get(2);
			System.out.println("-----newProjectCorePath: " + newProjectCorePath);

			oldSystemProjectCorePath = oldSystemProjectPaths.get(2);
			System.out.println("-----oldSystemProjectCorePath: " + oldSystemProjectCorePath);

			newProjectWsPath = newProjectPaths.get(3);
			System.out.println("-----newProjectWsPath: " + newProjectWsPath);

			oldSystemProjectWsPath = oldSystemProjectPaths.get(3);
			System.out.println("-----oldSystemProjectWsPath: " + oldSystemProjectWsPath);

			newProjectWebPath = newProjectPaths.get(4);
			System.out.println("-----newProjectWebPath: " + newProjectWebPath);

			oldSystemProjectWebPath = oldSystemProjectPaths.get(4);
			System.out.println("-----oldSystemProjectWebPath: " + oldSystemProjectWebPath);

			newProjectWebappPath = newProjectPaths.get(5);
			System.out.println("-----newProjectWebappPath: " + newProjectWebappPath);

			oldSystemProjectWebContentPath = oldSystemProjectPaths.get(5);
			System.out.println("-----oldSystemProjectWebContentPath: " + oldSystemProjectWebContentPath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error: " + e.getMessage());
		}
	}

	/**
	 * convertProject
	 * 
	 * @param path
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void convertProject(String path) throws Exception {
		File[] Files = new File(path).listFiles();
		try {
			for (File f : Files) {
				if (f.isDirectory()) {
					fileAndFloderRename(f, readProjectName, newProjectName, "Floder", oldFloderName, newFloderName);
					convertProject(f.getPath());
				}
				if (f.isFile()) {
					allFiles.add(f);
				}
			}
			// End for loop
			// do trans file content
			if (allFiles.size() > 0) {
				fileContextTrans(allFiles, AcceptFileType, readProjectName, newProjectName, oldFloderName,
						newFloderName);
				allFiles = new ArrayList<File>();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"convertProject\" Err : " + e.getMessage());
		}
	}

	/**
	 * mergeOldAndNewProject
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void mergeOldAndNewProject(String path) throws Exception {

		File[] oldSys_ScanFiles = new File(path).listFiles();
		try {
			for (File f : oldSys_ScanFiles) {
				if (f.isDirectory()) {
					mergeFileAndFloderRename(f, oldSystemProjectName, newProjectName, "Floder", oldSystemFloderName,
							newFloderName);
					mergeOldAndNewProject(f.getPath());
				}
				if (f.isFile()) {
					allFiles.add(f);
				}
			}
			// End for loop
			// do trans file content
			if (allFiles.size() > 0) {
				fileContextTrans(allFiles, mergeAcceptFileType, oldSystemProjectName, newProjectName,
						oldSystemFloderName, newFloderName);
				allFiles = new ArrayList<File>();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"allFile\" Err : " + e.getMessage());
		}
	}

	/**
	 * mergeFileAndFloderRename
	 * 
	 * @param file
	 * @param beforeProjectName
	 * @param afterProjectName
	 * @param fileType
	 * @param beforeFloderName
	 * @param afterFloderName
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void mergeFileAndFloderRename(File file, String beforeProjectName, String afterProjectName,
			String fileType, String beforeFloderName, String afterFloderName) throws Exception {
		try {
			String oldfileName = file.getName();
			String oldFilePath = file.getPath().replace("\\", "/");
			String newFilePath = "";
			File newFile = null;
			for (int i = 0; i < oldSystemProjectPaths.size(); i++) {
				if (oldFilePath.contains(oldSystemProjectPaths.get(i))) {
					newFilePath = oldFilePath.replace(oldSystemProjectPaths.get(i), newProjectPaths.get(i));
					newFilePath = newFilePath.replace(beforeProjectName, afterProjectName)
							.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
							.replace("YBD", "VBD").replace("ybd", "vbd")
							.replace("YCM", "VCM").replace("ycm", "vcm")
							.replace("YCT", "VCT").replace("yct", "vct")
							.replace("YDD", "VDD").replace("ydd", "vdd")
							.replace("YIM", "VIM").replace("yim", "vim")
							.replace("YPM", "VPM").replace("ypm", "vpm")
							.replace("YST", "VST").replace("yst", "vst")
							.replace("Svst","Syst" );
					mergeNewFilePath = newFilePath;
					break;
				}
			}
			fileAndFloderRenameChkFileType(file, fileType, oldfileName, beforeProjectName, newFile, newFilePath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"fileRename\" Err : " + e.getMessage());
		}
	}

	/**
	 * fileAndFloderRename
	 * 
	 * @param file
	 * @param beforeProjectName
	 * @param afterProjectName
	 * @param fileType
	 * @throws Exception
	 */
	public static void fileAndFloderRename(File file, String beforeProjectName, String afterProjectName,
			String fileType, String beforeFloderName, String afterFloderName) throws Exception {
		try {
			String fileName = file.getName();
			String oldFilePath = file.getPath();
			// newPath
			String newFilePath = "";
			newFilePath = oldFilePath.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
					.replace(beforeProjectName, afterProjectName).replace(beforeFloderName, afterFloderName);
			newFilePath = fileAndFloderNameReplaceCheck(newFilePath, beforeProjectName, afterProjectName);
			File newFile = null;
			File newDirPath = new File(newPath);
			if (!newDirPath.exists()) {
				newDirPath.mkdir();
			}
			fileAndFloderRenameChkFileType(file, fileType, fileName, beforeProjectName, newFile, newFilePath);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"fileRename\" Err : " + e.getMessage());
		}
	}

	/**
	 * fileAndFloderRenameChkFileType
	 * 
	 * @param file
	 * @param fileType
	 * @param fileName
	 * @param beforeProjectName
	 * @param newFile
	 * @param newFilePath
	 */
	public static void fileAndFloderRenameChkFileType(File file, String fileType, String fileName,
			String beforeProjectName, File newFile, String newFilePath) {
		try {
			if ("Floder".equals(fileType)) {
				// ex: EXT => VBD
				if (fileName.contains(beforeProjectName)) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println(
							"Floder Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				}
				// ex: ext => vbd
				else if (fileName.contains(beforeProjectName.toLowerCase())) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println(
							"Floder Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				} else {// Other name
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.mkdir();
					}
					System.out.println("The Floder 【 " + fileName + " 】 no needed rename! 【 " + file.getPath()
							+ " 】 to 【 " + newFile.getPath() + " 】。");
				}
			} else {
				// Type == File
				// ex: EXT => VBD
				if (fileName.contains(beforeProjectName)) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println(
							"File Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				}
				// ex: ext => vbd
				else if (fileName.contains(beforeProjectName.toLowerCase())) {
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println(
							"File Rename Success! 【 " + file.getPath() + " 】 to 【 " + newFile.getPath() + " 】。");
				} else {// Other name
					newFile = new File(newFilePath);
					if (!newFile.exists()) {
						newFile.createNewFile();
					}
					System.out.println("The File 【 " + fileName + " 】 no needed rename! 【 " + file.getPath()
							+ " 】 to 【 " + newFile.getPath() + " 】。");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("【fileAndFloderRenameChkFileTypeErr】:" + e.getMessage());
		}

	}

	/**
	 * @param allFiles
	 * @param AcceptFileType
	 * @param beforeProjectName
	 * @param afterProjectName
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static void fileContextTrans(List<File> allFiles, String AcceptFileType[], String beforeProjectName,
			String afterProjectName, String beforeFloderName, String afterFloderName) throws Exception {
		String acpFileType = "";
		String thisFileType = "";

		BufferedReader br = null;
		String line = "";

		FileWriter fw = null;

		try {
			if (AcceptFileType.length == 0) {
				throw new Exception("AcceptFileType's length is empty !!");
			} else {
				for (int i = 0; i < AcceptFileType.length; i++) {
					acpFileType = AcceptFileType[i];
					for (File thisFile : allFiles) {
						// chk fileType
						thisFileType = thisFile.getName().substring(thisFile.getName().lastIndexOf("."),
								thisFile.getName().length());
						thisFileType = thisFileType.toLowerCase();
						if (!acpFileType.equals(thisFileType)) {
							continue;
						} else {
							// if file is .rar&zip&png&jpg&css&js&cab type
							if (".rar".equals(thisFileType) || ".zip".equals(thisFileType)
									|| ".png".equals(thisFileType) || ".jpg".equals(thisFileType)
									|| ".css".equals(thisFileType) || ".js".equals(thisFileType)
									|| ".cab".equals(thisFileType) || ".jrxml".equals(thisFileType)
									|| ".jasper".equals(thisFileType)) {
								String thisFilePath = thisFile.getPath();
								String newFilePaths = thisFile.getPath().replace(beforeProjectName, afterProjectName)
										.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
										.replace(beforeFloderName, afterFloderName);
								newFilePaths = fileAndFloderNameReplaceCheck(newFilePaths, beforeProjectName,
										afterProjectName);
								File oldFile = new File(thisFilePath);
								if (!oldFile.exists()) {
									System.out.println("old file is no exists!!");
								}
								File newFile = new File(newFilePaths);
								if (newFile.exists()) {
									newFile.delete();
								}
								try {
									FileUtils.copyFile(oldFile, newFile);
									System.out.println("\"Copy file success!! \" 【 " + oldFile.getPath() + " to 【 "
											+ newFile.getPath() + " 】 。");
									continue;
								} catch (Exception e) {
									e.printStackTrace();
									System.out.println("\"Copy file failed!! \"" + e.getMessage());
								}
							}
							if (thisFile.getName().contains(beforeProjectName)
									|| thisFile.getName().contains(beforeProjectName.toLowerCase())
									|| thisFile != null) {
								// file rename
								if ("merge".equals(RunMode)) {
									mergeFileAndFloderRename(thisFile, beforeProjectName, afterProjectName, "File",
											beforeFloderName, afterFloderName);

								} else {
									fileAndFloderRename(thisFile, beforeProjectName, afterProjectName, "File",
											beforeFloderName, afterFloderName);
								}
							} else {
								throw new Exception(thisFile.getPath() + "檔案為NULL");
							}
							File mergeNewFile = new File(mergeNewFilePath);
							if(mergeNewFile.exists() &&
							   mergeNewFile.length()>0) {
								System.out.println("【 略過 新系統: "+newProjectName+" 目錄: "+mergeNewFilePath +"已有同名的檔案 !! 】");
								continue;
							}
							br = new BufferedReader(new FileReader(thisFile));
							String lineTmp;
							while ((line = br.readLine()) != null) {
								lineTmp = line;
								// line contain "EXT"
								if (lineTmp.contains(beforeProjectName)) {
									lineTmp = lineTmp.replace(beforeProjectName, afterProjectName);
									// line contain "ext"
								} 
								if (lineTmp.contains(beforeProjectName.toLowerCase())) {
									lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
											afterProjectName.toLowerCase());
								}

								if ("ext".equals(beforeProjectName.toLowerCase())) {
									if (lineTmp.contains("extends")) {
										lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
												afterProjectName.toLowerCase());
										lineTmp = lineTmp.replace(afterProjectName.toLowerCase() + "ends", "extends");
									} else {
										lineTmp = lineTmp.replace(beforeProjectName.toLowerCase(),
												afterProjectName.toLowerCase());
									}
									if (lineTmp.contains(afterProjectName.toLowerCase() + "en")) {
										lineTmp = lineTmp.replace(afterProjectName.toLowerCase() + "en",
												beforeProjectName.toLowerCase() + "en");
									}
									if (lineTmp.contains("T" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("T" + afterProjectName.toLowerCase(),
												"T" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("t" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("t" + afterProjectName.toLowerCase(),
												"t" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("N" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("N" + afterProjectName.toLowerCase(),
												"N" + beforeProjectName.toLowerCase());
									}
									if (lineTmp.contains("n" + afterProjectName.toLowerCase())) {
										lineTmp = lineTmp.replace("n" + afterProjectName.toLowerCase(),
												"n" + beforeProjectName.toLowerCase());
									}
								}

								if (lineTmp.contains("package lts.")) {
									lineTmp = lineTmp.replace("package lts.", "package mitac.");
								}
								if (lineTmp.contains("import gov.fdc.")) {
									lineTmp = lineTmp.replace("import gov.fdc.", "import mitac.");
								}
								if (lineTmp.contains("import lts.")) {
									lineTmp = lineTmp.replace("import lts.", "import mitac.");
								}
								if (lineTmp.contains("import com.acer")) {
									lineTmp = lineTmp.replace("import com.acer", "import com.mitac");
								}
								if (lineTmp.contains("com.acer")) {
									lineTmp = lineTmp.replace("com.acer", "com.mitac");
								}
								if (lineTmp.contains("FDCTransactionManager")) {
									lineTmp = lineTmp.replace("FDCTransactionManager", "MITACTransactionManager");
								}
								if (lineTmp.contains("LTSDao")) {
									lineTmp = lineTmp.replace("LTSDao", "MITACDao");
								}
								if (lineTmp.contains("extends " + beforeProjectName + "Controller")) {
									lineTmp = lineTmp.replace("extends " + beforeProjectName + "Controller",
											"extends CommonController");
								}
								if (lineTmp.contains("extends " + beforeProjectName.toLowerCase() + "Controller")) {
									lineTmp = lineTmp.replace(
											"extends " + beforeProjectName.toLowerCase() + "Controller",
											"extends CommonController");
								}
								if (lineTmp.contains("class=\"table-redmond\" width='100%'")) {
									lineTmp = lineTmp.replace("class=\"table-redmond\" width='100%'",
											"class=\"innerTable\"");
								}
								if (lineTmp.contains("gov.fdc.")) {
									lineTmp = lineTmp.replace("gov.fdc.", "mitac.");
								}
								if (lineTmp.contains("FdcThreadHolder")) {
									lineTmp = lineTmp.replace("FdcThreadHolder", "MitacThreadHolder");
								}
								if (lineTmp.contains("LTSAdapter")) {
									lineTmp = lineTmp.replace("LTSAdapter", "Adapter");
								}
								if (lineTmp.contains("LTSApplicationException")) {
									lineTmp = lineTmp.replace("LTSApplicationException", "MITACApplicationException");
								}
								if (lineTmp.contains("LTSDataAccessException")) {
									lineTmp = lineTmp.replace("LTSDataAccessException", "MITACDataAccessException");
								}
								if (lineTmp.contains("LTSManagerImpl")) {
									lineTmp = lineTmp.replace("LTSManagerImpl", "CommonManagerImpl");
								}
								if (lineTmp.contains("LTSWebManagerImpl")) {
									lineTmp = lineTmp.replace("LTSWebManagerImpl", "MITACWebManagerImpl");
								}
								if (lineTmp.contains("LTSWebManager")) {
									lineTmp = lineTmp.replace("LTSWebManager", "MITACWebManager");
								}
								if (lineTmp.contains("LTSManager")) {
									lineTmp = lineTmp.replace("LTSManager", "CommonManager");
								}
								if (lineTmp.contains("LTSLog")) {
									lineTmp = lineTmp.replace("LTSLog", "MITACLog");
								}
								if (lineTmp.contains("LTSLogConstant")) {
									lineTmp = lineTmp.replace("LTSLogConstant", "MITACLogConstant");
								}
								if (lineTmp.contains("LTSController")) {
									lineTmp = lineTmp.replace("LTSController", "MITACController");
								}
								if (lineTmp.contains("import mitac.global.api")) {
									lineTmp = lineTmp.replace("import mitac.global.api", "import mitac."+afterProjectName.toLowerCase()+".api");
								}
								if (lineTmp.contains("import mitac.global.core")) {
									lineTmp = lineTmp.replace("import mitac.global.core", "import mitac."+afterProjectName.toLowerCase()+".core");
									if(lineTmp.contains("MITACDao")||lineTmp.contains("Ende")||lineTmp.contains("PhraseUtil")) {
										lineTmp = lineTmp.replace("import mitac."+afterProjectName.toLowerCase()+".core","import mitac.global.core");
									}
								}
								if (lineTmp.contains("import mitac.global.batch")) {
									lineTmp = lineTmp.replace("import mitac.global.batch", "import mitac."+afterProjectName.toLowerCase()+".batch");
								}
								if (lineTmp.contains("lts.global.core.util.LTSButton")) {
									lineTmp = lineTmp.replace("lts.global.core.util.LTSButton", "mitac."+afterProjectName.toLowerCase()+".core.util.LTSButton1");
								}
								if (lineTmp.contains("LTSButton")) {
									if(!(lineTmp.contains("LTSButton1"))) {
										lineTmp = lineTmp.replace("LTSButton", "LTSButton1");
									}
								}
								fileContentTmp = fileContentTmp + lineTmp + "\n";
							}
						}
						// end while
						try {
							String oldFilePath = thisFile.getPath();
							String newFilePath = "";
							if ("merge".equals(RunMode)) {
								newFilePath = mergeNewFilePath;
							} else {
								newFilePath = oldFilePath
										.replace(beforeProjectName.toLowerCase(), afterProjectName.toLowerCase())
										.replace(beforeProjectName, afterProjectName)
										.replace(beforeFloderName, afterFloderName);
								newFilePath = fileAndFloderNameReplaceCheck(newFilePath, beforeProjectName,
										afterProjectName);
							}
							File newFile = new File(newFilePath);
							if (!newFile.exists()) {
								newFile.createNewFile();
							}
							fw = new FileWriter(newFile);
							fw.write("");
							fw.flush();
							fw.write(fileContentTmp);
							fw.flush();
							fw.close();
							fileContentTmp = "";
							System.out.println("Write File Success!!! " + newFile.getName());
						} catch (Exception e) {
							e.printStackTrace();
							System.out.println("\"FileWriter\" Err : " + e.getMessage());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Method \"fileContextTrans\" Err : " + e.getMessage());
		}
	}

	/**
	 * fileAndFloderNameReplaceCheck
	 * 
	 * @param fileOrFloderName
	 * @return
	 */
	public static String fileAndFloderNameReplaceCheck(String Path, String beforeProjectName, String afterProjectName) {
		String rtnPath = "";
		String fileName = "";
		if (!"".equals(Path)) {
			fileName = Path.substring(Path.lastIndexOf("\\") + 1, Path.length());
			if ("ext".equals(beforeProjectName.toLowerCase())) {
				if (fileName.contains(afterProjectName.toLowerCase() + "end")) {
					fileName = fileName.replace(afterProjectName.toLowerCase() + "end",
							beforeProjectName.toLowerCase() + "end");
				}
				if (fileName.contains("T" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("T" + afterProjectName.toLowerCase(),
							"T" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("t" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("t" + afterProjectName.toLowerCase(),
							"t" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("N" + afterProjectName)) {
					fileName = fileName.replace("N" + afterProjectName.toLowerCase(),
							"N" + beforeProjectName.toLowerCase());
				}
				if (fileName.contains("n" + afterProjectName.toLowerCase())) {
					fileName = fileName.replace("n" + afterProjectName.toLowerCase(),
							"n" + beforeProjectName.toLowerCase());
				}
			}
			rtnPath = Path.substring(0, Path.lastIndexOf("\\") + 1) + fileName;
		}

		return rtnPath;
	}

	// 讀取設定檔
	public static Properties loadProperties(Properties properties, String propertiesFileName) {

		String configFilePath = "src/configure/" + propertiesFileName + ".properties";
		try {
			properties.load(new FileInputStream(new File(configFilePath)));
		} catch (FileNotFoundException e) {
			System.out.println("【ERROR】" + configFilePath + ",  " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("【ERROR】" + configFilePath + ",  " + e.getMessage());
			e.printStackTrace();
		}
		return properties;
	}

	// 要轉換系統的舊路徑
	public static String getOldPath() {
		return UtilConfig.getProperty("oldPath");
	}

	// 要放到的新路徑
	public static String getNewPath() {
		return UtilConfig.getProperty("newPath");
	}

	// 要合併舊系統的路徑
	public static String getOldSysPath() {
		return UtilConfig.getProperty("oldSysPath");
	}

	// 舊資料夾名稱
	public static String getOldFloderName() {
		return UtilConfig.getProperty("oldFloderName");
	}

	// 新資料夾名稱
	public static String getNewFloderName() {
		return UtilConfig.getProperty("newFloderName");
	}

	// 舊系統資料夾名稱
	public static String getOldSystemFloderName() {
		return UtilConfig.getProperty("oldSystemFloderName");
	}

	// 要讀取的專案名稱
	public static String getReadProjectName() {
		return UtilConfig.getProperty("readProjectName");
	}

	// 要變成的專案名稱
	public static String getNewProjectName() {
		return UtilConfig.getProperty("newProjectName");
	}

	// 舊系統的專案名稱
	public static String getOldSystemProjectName() {
		return UtilConfig.getProperty("oldSystemProjectName");
	}
}
