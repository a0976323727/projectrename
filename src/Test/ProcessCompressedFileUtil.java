package Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ProcessCompressedFileUtil {
	// 壓縮檔案的目錄
	public static String CompressedFilePath = "src\\Test\\resource";

	// 壓縮檔資料暫存目錄
	public static String CompressedTempPath = "Temp";

	// 系統日期 年月日時分秒
	public static String now = getNow();
	
	// 解壓縮檔案
	public static void deCompression(String deCompressedFileName) {
		File[] fileList = new File(CompressedFilePath).listFiles();
		String thisFileName = "";
		String thisFileType = "";
		String thisFilePath = "";
		for(File f : fileList) {
			thisFileName = f.getName().substring(0, f.getName().lastIndexOf("."));
			thisFileType = f.getName().substring(f.getName().lastIndexOf("."),f.getName().length());
			thisFilePath = f.getPath();
			if(deCompressedFileName.equals(thisFileName)) {
				System.out.println("Fine the file like: " + thisFileName + " File path: "+ thisFilePath);
				break;
			}
		}
		if(".rar".equals(thisFileType) || ".RAR".equals(thisFileType)){
			System.out.println("This file's type is: " + thisFileType);
			File outFile = new File(CompressedTempPath+File.separator+"tempRAR_"+now);
			deCompressionMethod(thisFilePath, outFile);
			
		}else {
			System.out.println("This file's type is: " + thisFileType);
			File outFile = new File(CompressedTempPath+File.separator+"tempZIP_"+now);
			deCompressionMethod(thisFilePath, outFile);
			processCompressionFiles(outFile.getName());
		}
		
	}

	// 處理壓縮檔案 PS修改檔案名稱、檔案內容
	public static void processCompressionFiles(String tempFloderName) {

	}

	// 壓縮檔案
	public static void Compression(String tempFloderName) {
		
	}
	
	// 取得系統日期 年月日時分秒
	public static String getNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		Date d = new Date();
		String now = sdf.format(d);
		return now;
	}
	// 解壓縮方法
	public static void deCompressionMethod(String CompressedFilePath, File outFile) {
		if(outFile.exists()) {
		   outFile.mkdir();
		}
		try {
			ZipFile zipFile = new ZipFile(CompressedFilePath);
			Enumeration en = zipFile.entries();
			ZipEntry zipEntry = null;
			while (en.hasMoreElements()) {
				zipEntry = (ZipEntry) en.nextElement();
				if (zipEntry.isDirectory()) {
					// mkdir directory
					String dirName = zipEntry.getName();
					// System.out.println("=dirName is:=" + dirName + "=end=");
					dirName = dirName.substring(0, dirName.length() - 1);
					File f = new File(outFile.getPath() + File.separator + dirName);
					f.mkdirs();
				} else {
					// unzip file
					String strFilePath = outFile.getPath() + File.separator + zipEntry.getName();
					File f = new File(strFilePath);
					// /////begin/////
					// 判斷檔案不存在的話,就建立該檔案所在資料夾的目錄
					if (!f.exists()) {
						String[] arrFolderName = zipEntry.getName().split("/");
						String strRealFolder = "";
						for (int i = 0; i < (arrFolderName.length - 1); i++) {
							strRealFolder += arrFolderName[i] + File.separator;
						}
						strRealFolder = outFile.getPath() + File.separator + strRealFolder;
						File tempDir = new File(strRealFolder);
						// 此處使用.mkdirs()方法,而不能用.mkdir()
						tempDir.mkdirs();
					}
					// /////end///
					f.createNewFile();
					InputStream in = zipFile.getInputStream(zipEntry);
					FileOutputStream out = new FileOutputStream(f);
					try {
						int c;
						byte[] by = new byte[1024];
						while ((c = in.read(by)) != -1) {
							out.write(by, 0, c);
						}
						// out.flush();
					} catch (IOException e) {
						throw e;
					} finally {
						out.close();
						in.close();
					}
				}
			}
			System.out.println("Done");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
