package Main;

import Util.ProjectRenameUtil;
import Util.ProjectRenameUtil_v2;

public class ProjectRenameMain {

	public static void main(String[] args) {
		//Run "convert or merge"
		//ProjectRenameUtil.run("merge");
		ProjectRenameUtil_v2.run("convert");
		ProjectRenameUtil_v2.run("merge");
	}

}
